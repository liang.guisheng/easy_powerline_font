# easy_powerline_font

Make install powerline font in linux easier

## Usage

```
git clone https://gitlab.com/theo-l/easy_powerline_font
cd easy_powerline_font/
./easy_install.sh
```
